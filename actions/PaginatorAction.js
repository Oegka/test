export const CHANGE_NUMBER_PAGE = 'CHANGE_NUMBER_PAGE'

export function changePage(numberPage) {
    return {
        type: CHANGE_NUMBER_PAGE,
        number: numberPage
    }
}