export const CHANGE_PAGE = 'CHANGE_PAGE'
export const CHANGE_PAGE_WAIT = 'CHANGE_PAGE_WAIT'
export const SET_MAX_PAGE = 'SET_MAX_PAGE'
export const SET_MAX_PAGE_WAIT = 'SET_MAX_PAGE_WAIT'

const AMOUNT_CITIES = 10

export function changePage(numberPage) {

    return (dispatch) => {
        dispatch({
            type: CHANGE_PAGE_WAIT
        })

        return fetch(`http://pogoda.ngs.ru/api/v1/cities?limit=10&offset=${(numberPage -1) * AMOUNT_CITIES}`)
            .then(function(req) {
                return req.json()
            })
            .then(function(data) {
                //console.log(data.cities)
                dispatch({
                    type: CHANGE_PAGE,
                    cities: data.cities,
                    number:numberPage
                })
            })

    }
}

export function setMaxValuePage() {
    return (dispatch) => {

        dispatch({
            type: SET_MAX_PAGE_WAIT
        })

        return fetch(`http://pogoda.ngs.ru/api/v1/cities`)
            .then(function(req) {
                return req.json()
            })
            .then(function(data) {
                dispatch({
                    type: SET_MAX_PAGE,
                    maxPage: Math.ceil(data.metadata.resultset.count / AMOUNT_CITIES )
                })
            })

    }
}