export const GET_CITIES = 'GET_CITIES'
export const GET_CITIES_REQUEST = 'GET_CITIES_REQUEST'
import fetch from 'isomorphic-fetch';


export function getCities(offset) {

    return (dispatch) => {
        dispatch({
            type: GET_CITIES_REQUEST,
            cities: []
        })

        return fetch(`http://pogoda.ngs.ru/api/v1/cities?limit=10&offset=${offset}`)
            .then(function(req) {
                return req.json()
            })
            .then(function(data) {
                //console.log(data.cities)
                dispatch({
                    type: GET_CITIES,
                    cities: data.cities
                })
            })
    }


}