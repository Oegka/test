const initialState = {
    number: 0,
    cities: [],
    fetching: false,
    maxPage: 0,
    maxPageReady: false
}


export default function page(state = initialState,action) {

    switch(action.type) {
        case 'CHANGE_PAGE':
            return  Object.assign({}, state, {
                cities: cloneDeep(sort(action.cities,function(city) { return city.title })),
                number: action.number,
                fetching: false
            })
        case 'GET_CITIES_REQUEST':
            return Object.assign({}, state, {
                cities: [],
                fetching: true
            })
        case 'SET_MAX_PAGE':
            return Object.assign({}, state, {
                maxPage: action.maxPage,
                maxPageReady: true
            })
        case 'SET_MAX_PAGE_WAIT':
            return Object.assign({}, state, {
                maxPageReady: false
            })
        default:
            return state
    }
}