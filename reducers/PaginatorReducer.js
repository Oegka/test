const initialState = {
    page: 1
}


export default function paginator(state = initialState,action) {

    switch(action.type) {
        case 'CHANGE_NUMBER_PAGE':
            return  Object.assign({}, state, {
                page: action.number
            })
        default:
            return state
    }
}