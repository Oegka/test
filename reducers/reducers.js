import _ from 'lodash'

const initialState = {
    cities: [],
    fetching: false
}


export default function todoApp(state = initialState,action) {

    switch(action.type) {
        case 'GET_CITIES':
            return  Object.assign({}, state, {
                cities: _.cloneDeep(action.cities),
                fetching: false
            })
        case 'GET_CITIES_REQUEST':
            return Object.assign({}, state, {
                cities: action.cities,
                fetching: true
            })
        default:
            return state
    }
}