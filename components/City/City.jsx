import React, { Component, PropTypes } from 'react'
import classNames from 'classnames/bind'
import styles from './City.css'

let cx = classNames.bind(styles);


export default class City extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectCity: false
        };
    }

    componentDidMount() {
        $(this.refs.city)
            .accordion()
        ;
    }

    render() {

        let listClass = cx({
            'ui': true,
            'purpl': this.state.selectCity,
            'segment': true
        })

        return(
            <div ref = 'city' className = {listClass} onMouseEnter = {e => this.selectionChanges(e)} onMouseLeave = {e => this.selectionChanges(e)}>
                <div className = 'ui accordion'>
                    <div className = 'title'>
                        <i className = 'dropdown icon'></i>
                        {this.props.City.title}
                    </div>
                    <div className = 'content'>
                        <div className = 'transition hidden'>
                            <div className = 'ui middle aligned divided list'>
                                {Object.keys(this.props.City).map(function(key) {
                                    return <div className = 'item'>
                                        <b>{key}</b>: {this.props.City[key]}
                                    </div>
                                }.bind(this))}

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }

    selectionChanges(e) {
        switch(e.type) {
            case 'mouseenter':
                this.setState({
                    selectCity: true
                });
                break;
            case 'mouseleave':
                this.setState({
                    selectCity: false
                });
        }

    }
}