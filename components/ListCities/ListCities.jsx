import React, { Component, PropTypes } from 'react'
import City from 'City/City.jsx'


export default class ListCities extends Component {

    render() {
        return(
            <div className = 'ui segments '>
                {this.props.list.map(function(city) {
                    return <City City = {city} getCities = {this.props.getCities}/>
                }.bind(this))}
        </div>
        )
    }
}