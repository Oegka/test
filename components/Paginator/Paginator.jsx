import React, { Component, PropTypes } from 'react'
import styles from './Paginator.css'


export default class Paginator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectPage: 1,
            maxCount: 20,
            isOffset: false,
            array: []
        }
        this.range = this.range.bind(this)

    }

    componentWillMount() {
        console.log('will mount')
        this.setState({
            array: cloneDeep(this.range(3)),
            isOffset: true
        })
    }

    /*componentDidMount() {
        this.setState({
            isOffset: false
        })
    }*/

    componentWillReceiveProps(nextProps) {

        if(this.props.selectPage != nextProps.selectPage) {

            if (nextProps.selectPage - 2 > 0 && nextProps.selectPage + 2 < +this.props.max ) {
                this.setState({
                    array: cloneDeep(this.range(nextProps.selectPage)),
                    isOffset: true
                })
            }
            else {

            }
        }
    }

    range(referenceNum) {
        var array = []
        for (let start = referenceNum -2; start <= referenceNum + 2; start++) {
            array.push(start)
        }
        return array;
    }

    shouldComponentUpdate(nextProps,nextState) {
        console.log('should' + nextState.isOffset)
        return nextState.isOffset
    }

    componentWillUpdate(newProps,nextState) {
        this.setState({
            isOffset: false
        })
    }

    render() {
            console.log('render')
            return (
                <div className="ui grid" style = {styles}>

                    <div className="three column row">
                        <div className="right aligned four wide column">
                            <div className ="ui teal basic label direct left" onClick = {e => this.change(this.props.selectPage - 1)}>
                                <i className="chevron circle left icon"></i>Предыдущая
                            </div>
                        </div>
                        <div className="center aligned eight wide column">
                            <div className = 'ui teal circular labels'>
                                {this.state.array.map(function(item) {
                                    return <a className = 'ui basic label' onClick = {e => this.change(+e.target.textContent)}>{item}</a>
                                }.bind(this))}
                            </div>
                        </div>
                        <div className="four wide column">
                            <div className ="ui teal basic label direct right" onClick = {e => this.change(this.props.selectPage + 1)}>
                                Следующая<i className="chevron circle right icon"></i>
                            </div>
                        </div>
                    </div>

                </div>
            )
    }

    change(nextPage) {
        this.props.changePage(nextPage)
    }


}