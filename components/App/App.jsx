import React, { Component, PropTypes } from 'react'
import ListCities from 'ListCities/ListCities.jsx'
import styles from './App.css'
import * as Pro from '../action.js'
import classNames from 'classnames/bind'

export default class App extends Component {

    render() {
        let classes = classNames({
            'ui': true,
            'segment': true
        });
        const { cities } = this.props
        //let cities = ['Novosibirsk','Moskow','Piter']
        const { dispatch, select } = this.props
        const {addCity} = this.props.Pro

        return(
            <div id = 'app' className = {classes} style = {styles} onClick = {() => this.props.Pro.addCity('Tomsk')}>
                <ListCities list = {cities} prum = {addCity}/>
            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        cities: state.city
    }
}
function mapDispatchToProps(dispatch) {
    return {
        Pro: bindActionCreators(Pro, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
