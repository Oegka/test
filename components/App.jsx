import React, { Component, PropTypes } from 'react'
import ListCities from 'ListCities/ListCities.jsx'
import Paginator from 'Paginator/Paginator.jsx'
import styles from './App.css'
import * as pageActions from '../actions/PageAction.js'
import classNames from 'classnames/bind'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

export default class App extends Component {

    componentWillMount() {
        console.log('mount app')
        this.props.PageActions.changePage(1)
        this.props.PageActions.setMaxValuePage()
    }

    shouldComponentUpdate(nextProps,nextState) {
        console.log(nextProps.Page.number != this.props.Page.number + 'ap')
        return nextProps.Page.number != this.props.Page.number || this.props.Page.maxPage == 0
    }

    render() {
        console.log('render app')
        let classes = classNames({
            'ui': true,
            'segment': true
        });
        const {cities,number,maxPage} = this.props.Page
        const {changePage} = this.props.PageActions
        return(
            <div id = 'app' className = {classes} style = {styles}>
                <a className = "ui teal right ribbon label">Страница {number}</a>
                <ListCities list = {cities} />

                <Paginator changePage = {changePage}  max = {maxPage} selectPage = {number}/>
            </div>

        )
    }
}

function mapStateToProps (state) {
    return {
        Page: state.page
    }
}
function mapDispatchToProps(dispatch) {
    return {
        PageActions: bindActionCreators(pageActions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
